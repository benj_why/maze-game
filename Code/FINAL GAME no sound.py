from kivy.graphics import *
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.core.window import Window
from kivy.core.audio import SoundLoader
from kivy.uix.widget import Widget
from kivy.clock import Clock
import sqlite3
import math
import time
import json

#Importing functions from Kivy

class Account(Widget):
    def __init__(self,**kwargs):
        super(Account, self).__init__(**kwargs)
        self.Username = kwargs['Username']
        self.Password = kwargs['Password']
        self.Level_Up_To = kwargs['Level_Up_To']
        self.ID = kwargs['ID']
        #Creates account class with empty values (to be changed once instantiated)
    

class OpenScreen(Screen):
    def __init__(self,**kwargs):
        super(OpenScreen, self).__init__(**kwargs)
        Start = ImageButton(source = 'background.png', Page = '')#Creates button
        Start.bind(on_press=self.changer) #Binds an action to the press
                                          # of the button (calls changer())
                                          
        self.add_widget(Start)  #Adds button to screen
        self.add_widget(Label(font_name  = 'FFFFORWA.TTF', font_size = 75, text = 'LIGHTS OUT'))
    def changer(self, *args):
        
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
                    
        self.manager.current = 'LOGIN'
    #Function which switches the current screen to the menu screen,
    # of which the name is 'MS', which has already been added to the
    # screen manager

#Creates a Screen widget called OpenScreen


class MenuScreen(Screen):
    def __init__(self,**kwargs):
        super(MenuScreen, self).__init__(**kwargs)

        self.add_widget(Image(source = 'background.png',size_hint = (None,None),size = [Window.size[0]*2,Window.size[1]*2], pos = [-200,-300]))
        self.BL = BoxLayout(orientation = 'vertical') # Adds a box layout (vertically)

        self.LevelsButton = ImageButton(source = 'Levels.png', Page = 'LEVELS')
        self.LevelsButton.bind(on_press = self.change_screen)
        
        self.TutorialButton = ImageButton(source = 'TutorialButton.png',Page = 'Tutorial')
        self.TutorialButton.bind(on_press = self.tut_screen)
        # Creates 3 buttons, one for each option
                                          
        self.BL.add_widget(self.LevelsButton)
        self.BL.add_widget(self.TutorialButton)

        # Adds buttons to the box layout so they're ordered vertically

        self.add_widget(self.BL) # Adds the layout to the screen widget so it will
                            # be displayed when the screen is run

    def create_level_screen(self,*args):
        SM.LVS = LevelsScreen(name = 'LEVELS')
        SM.add_widget(SM.LVS)
    #Creates levels screen


    def change_screen(self, *args):
        SM.LVS.destroy()
        SM.LVS.create()
        '''Click = SoundLoader.load('Button.MP3')
        if Click:
            Click.play() #Activates button click sound'''
        SM.current = 'LEVELS'            

    #Refreshses level screen then switches to it

    def UpdateLogin(self,*args):
        self.BL.remove_widget(self.LevelsButton)
        self.BL.remove_widget(self.TutorialButton)
        self.BL.add_widget(Label(font_name  = 'FFFFORWA.TTF', text = args[0], font_size = 40, size = [500,30])) #Adds username to top of box
        self.BL.add_widget(self.LevelsButton)
        self.BL.add_widget(self.TutorialButton)
        #Recreates box layout but with username on top

    def tut_screen(self,*args):
        SM.current = 'TUTORIAL'
        #Takes user to tutorial screen

class TutorialScreen(Screen):
    def __init__(self,**kwargs):
        super(TutorialScreen, self).__init__(**kwargs)

        self.add_widget(Image(source = 'background.png',size_hint = (None,None),size = [Window.size[0]*2,Window.size[1]*2], pos = [-200,-300]))
        self.add_widget(Image(source = 'Tutorial.png'))

        #Adds background and tutorial image to screen
        
        Back = ImageButton(source = 'Menu.png',Page = '')
        Back.size_hint = (None,None)
        Back.size = [100,100]
        Back.pos = [Window.size[0]-100,0]
        Back.bind(on_press = self.back_menu)

        #Adds and positions Back button to take user back to menu
        
        FL = FloatLayout()
        FL.add_widget(Back)
        
        self.add_widget(FL)

    def back_menu(self,*args):
        SM.current = 'MENU'
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''

        #Takes user back to main menu
    
class TextBox(TextInput):
    def __init__(self,**kwargs):
        super(TextBox, self).__init__(**kwargs)
        self.cursor_blink = False
        self.background_color = [0,0,0,1.0]
        self.border = [0,255,0,1]
        self.cursor_color = [0,255,0,1]
        self.font_size = 50
        self.foreground_color = [0,255,0,1]
        self.hint_text_color = [255,255,255,1]
        self.multiline = False
        self.padding = [10,10,10,10]


class ImageButton(ButtonBehavior, Image):
    def __init__(self,**kwargs):
        super(ImageButton, self).__init__(**kwargs)
        self.page = kwargs['Page']

    #Creates ImageButton class and sets page attribute to be equal to parameter
    #set when instantiating button

class SignUpScreen(Screen):
    def __init__(self,**kwargs):
        super(SignUpScreen, self).__init__(**kwargs)

        self.add_widget(Image(source = 'background.png',size_hint = (None,None),size = [Window.size[0]*2,Window.size[1]*2], pos = [-200,-300]))
        
        self.Logged_In = False #Initially not logged in
        
        Box = BoxLayout(orientation = 'vertical')
        self.Username = TextBox(hint_text = 'Username')
        self.Password = TextBox(hint_text = 'Password', password = True)

        #Creates box layout and username and password input boxes
        
        Login = ImageButton(source = 'Login.png',Page = 'LOGIN')
        Login.bind(on_press = self.changeScreen)
        Create = ImageButton(source = 'CreateAccount.png',Page = 'LOGIN')
        Create.bind(on_press = self.Create_Account)

        #Creates the back and logon button, and gives them functionality
        #when they're pressed
        
        Box.add_widget(self.Username)
        Box.add_widget(self.Password)
        Box.add_widget(Create)
        Box.add_widget(Login)
        self.add_widget(Box)

        #Adds all the widgets into the box, then into the screen

    
    def Create_Account(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        
        connection = sqlite3.connect('MazeGameDatabase.db')
        cursor = connection.cursor()
        #Establishes connection to database and creates cursor to execute commands
        Results = ('''SELECT Username FROM Users WHERE Username = '{Name}' ''').format(Name = self.Username.text)
        cursor.execute(Results)
        #Checks if the inputted username is already present in the users table
        check = cursor.fetchall() #Returns list with all results of search
        if len(check) == 0 and self.Password.text != '':#If the username doesn't already exists
            
            connection.text_factory = str #sets any values read off or read into database to be a string, not unicode
            sql_command_unformatted = '''INSERT INTO Users (UserID, Username, Password, LevelUpTo)
                                        VALUES (NULL, "{Name}","{Password}","{Level}");'''
            #Raw SQL command, unformatted with values from python
            sql_command = sql_command_unformatted.format(Name = self.Username.text, Password = self.Password.text, Level = 1)
            cursor.execute(sql_command)
            #Formats SQL with values from sign up page and executes it, creating account using username and password input
            #And automatically setting the 1st level as the only one unlocked
            connection.commit()#Saves changes to database

            sql_command = ('''SELECT UserID FROM Users WHERE Username = '{Name}' ''').format(Name = self.Username.text)
            cursor.execute(sql_command) 
            PlayerID = cursor.fetchone()#Selects userID for new account
            
            self.User = Account(Username = self.Username.text,Password = self.Password.text, Level_Up_To = 1, ID = PlayerID[0] )
            #Creates account class and fills it with values from newly created account

            self.Logged_In = True #Changed as account created and logged on
            SM.MS.UpdateLogin(self.User.Username) #Updates menu screen with username as parameter
            SM.MS.create_level_screen() #Creates levels screen
            self.back()



        else: #If username exists
            Exist = ImageButton(source = 'Exist.png', Page = '')
            Exist.bind(on_press = self.removebutton)
            self.add_widget(Exist)#Creates error message (as a button, so it can be clicked to be removed)

        
        connection.close()
        #Saves changes and closes connection to database


    def removebutton(self,*args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.remove_widget(args[0])#Removes whichever button activated function
        
    def changeScreen(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.manager.current = 'LOGIN'

    #Changes display back to login

    def back(self,*args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.manager.current = 'MENU'
        


class LoginScreen(Screen):
    def __init__(self,**kwargs):
        super(LoginScreen, self).__init__(**kwargs)

        self.add_widget(Image(source = 'background.png',size_hint = (None,None),size = [Window.size[0]*2,Window.size[1]*2], pos = [-200,-300]))
        
        Box = BoxLayout(orientation = 'vertical')
        self.Username = TextBox(hint_text = 'Username')
        self.Password = TextBox(hint_text = 'Password', password = True)

        #Creates box layout and username and password input boxes
        

        Login = ImageButton(source = 'Login.png',Page = 'LOGIN')
        Login.bind(on_press = self.Logon)

        #Creates the logon button, and gives functionality
        #when pressed

        SignUpPage = ImageButton(source = 'SignUp.png',Page ='SIGNUP')
        SignUpPage.bind(on_press = self.change_screen)
        #Switches to sign up page
        
        Box.add_widget(self.Username)
        Box.add_widget(self.Password)
        Box.add_widget(Login)
        Box.add_widget(SignUpPage)
        self.add_widget(Box)

        #Adds all the widgets into the box, then into the screen

    def Logon(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
            
        connection = sqlite3.connect('MazeGameDatabase.db')
        connection.text_factory = str #sets any values read off or read into database to be a string, not unicode
        cursor = connection.cursor()
        #Establishes connection to database and creates cursor to execute commands
        Results = ('''SELECT * FROM Users WHERE Username = '{Name}' ''').format(Name = self.Username.text)
        cursor.execute(Results)
        #Checks if the username and password match the username and password of an account in the database
        check = cursor.fetchall() #Returns list with all results of search
        if len(check) == 1 and check[0][1] != '':#If the username exists
            if check[0][2] == self.Password.text:#Checks correct password's been input for the username
                SM.SignUp.User = Account(Username = self.Username.text,Password = self.Password.text, Level_Up_To = check[0][3], ID = check[0][0])
                #Creates account class and fills it with values from account

                self.Logged_In = True #Changed as account created and logged on
                
                SM.MS.UpdateLogin(SM.SignUp.User.Username) #Updates menu screen with username as parameter
                SM.MS.create_level_screen() #Creates levels screen

                self.back()

            else:#Wrong password error message
                self.Wrong = ImageButton(source = 'WrongPassword.png', Page = '')
                self.Wrong.bind(on_press = self.removebutton)
                self.add_widget(self.Wrong)#Creates error message


        else: #If doesn't exist
            self.NotExists = ImageButton(source = 'NotExists.png', Page = '')
            self.NotExists.bind(on_press = self.removebutton)
            self.add_widget(self.NotExists)#Creates error message (as a button, so it can be clicked to be removed)

        
        connection.close()
        #Saves changes and closes connection to database

    def removebutton(self,*args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.remove_widget(args[0])#Removes whichever button activated function

    def back(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.manager.current = 'MENU'

    #Changes display back to main menu

    def change_screen(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.manager.current = 'SIGNUP'
    #Changes display to different screen based off button 'page' attribute

class LevelsScreen(Screen):
    def __init__(self,**kwargs):
        super(LevelsScreen, self).__init__(**kwargs)

        self.add_widget(Image(source = 'background.png',size_hint = (None,None),size = [Window.size[0]*2,Window.size[1]*2], pos = [-200,-300]))

        self.create()

    def create(self,*args): #Creates screen
        self.Box = BoxLayout(orientation = 'vertical')
        self.Grid = GridLayout(cols = 5)
        Levels = ['Level1','Level2','Level3','Level4','Level5',
                  'Level6','Level7','Level8','Level9','Level10']
        for i in range(0,len(Levels)):
            if SM.SignUp.User.Level_Up_To >= i+1:#Only adds following if level is unlocked

                Levels[i] = ImageButton(source = 'L{0}.png'.format(str(i+1)), Page = str(i+1))

            #Sets level button source depending on which iteration it is
                Levels[i].bind(on_press = self.SetInitialLevel)
            else:
                Levels[i] = ImageButton(source = 'Locked.png', Page = '')
                Levels[i].bind(on_press = self.NotUnlocked)
            self.Grid.add_widget(Levels[i])

        #Creates a grid and then loop creates 10 buttons from list,
        #attaches bind function, then adds them to the grid
            
        self.Back = ImageButton(source = 'Menu.png', Page = 'MENU')
        self.Back.bind(on_press = self.back)

        #Creates back button and links it to function sending user back to menu
        
        self.Box.add_widget(self.Grid)
        self.Box.add_widget(self.Back)
        self.add_widget(self.Box)

        #Adds grid to a box layout,then adds a back button beneath that and
        #adds the box layout to the screen

    def destroy(self,*args):
        self.remove_widget(self.Box) #Clears screen ready for refreshing
        

    def NotUnlocked(self,*args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
            
        self.Locked = ImageButton(source = 'LevelLocked.png', Page = '')
        self.Locked.bind(on_press = self.removebutton)
        self.add_widget(self.Locked)

    def removebutton(self,*args):
        self.remove_widget(args[0])#Removes whichever button activated function
    
    def back(self, *args):
        self.manager.current = 'MENU'

    def SetInitialLevel(self, *args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.current_level = int(args[0].page)
        self.RunLevel()
        
        #Sets the current level as the value on the button pressed

    def RunLevel(self,*args):
        self.ReadLevels()

        #Reads all levels anew
        
        self.Level = LV()
        SM.add_widget(self.Level)
        SM.current = 'Level'

        #Instantiates the level screen class and adds it to the
        #screen manager, then switches the display to it

    def ReadLevels(self):
        Lists = open('Levels.txt','r')
        self.Levels_list = [[],[],[],[],[],[],[],[],[],[]]
        Temp_list = []


        #Sets empty lists for a temporary list to contain each level as it's read
        # and an overall list to contain all the levels
    
        for i in range(0,1000000):
            Temp_list.append(Lists.readline().strip().split(','))

        #Cycles through the first 2 mazes and appends each number into the list
        #Seperating the list out at the commas
        
        for i in range(0,10):
            for j in range(0,len(Temp_list[0])):
                           self.Levels_list[i].append(Temp_list.pop(0))
            #Cycles through lists/rows of maze up until the end of one maze
            #Removing them from the temp list and adding them as levels into the
            #Levels list
                           
class LV(Screen):
    def __init__(self, **kwargs):
        super(LV, self).__init__()
        self.name = 'Level'
        self.Level_Mask = Mask()
        self.add_widget(self.Level_Mask)

    #Creates LV screen, an instance of mask (and therefore gamescreen)
    #and adds it to the screen



class Mask(Widget):
    def __init__(self, **kwargs):
        super(Mask, self).__init__()

        self.Active = False
        
        with self.canvas:
            Color(0,0,0,1)# background colour/rectangle - black
            Rectangle(pos = [0,0], size=Window.size)
        #Creates places black rectangle over screen

        self.Start=ImageButton(source = 'StartButton.png',Page ='', size = [400,400])
        self.Start.pos = [(Window.size[0]/2)-(self.Start.size[0]/2),(Window.size[1]/2)-(self.Start.size[1]/2)]
        self.Start.bind(on_press=self.mask)
        self.add_widget(self.Start)
        #Creates start button, that when clicked, activates the mask function


    def mask(self,*args):
        '''Click = SoundLoader.load('Button.MP3')

        if Click:
            Click.play()'''
        self.Active = True
        with self.canvas:
            StencilPush()
            self.Torch = Ellipse(pos = self.Start.pos, size = [0,0])
            StencilUse()

            #Creates 'stencil', ellipse that's used as the hole in the mask
            self.GS = GameScreen(name = 'GS')
            StencilPop()
            #Sets gamescreen as the object being masked

        self.remove_widget(self.Start)
        #Removes start button (makes it dissapear)

        self.Torch.size = (self.GS.Player_Character.size[0]*4,self.GS.Player_Character.size[1]*4)
        self.Torch.pos = [(self.GS.Player_Character.center_x - (SM.LVS.Level.Level_Mask.Torch.size[0]/2)),((self.GS.Player_Character.center_y - (SM.LVS.Level.Level_Mask.Torch.size[1]/2)))]

        #Sets Torch size relative to character


        self.Timer = TimeScore(font_name = 'FFFFORWA.TTF', count = 0)
        #Creates Timer, sets count to 0

        self.Score_Board = Scoreboard(Total = self.GS.Total_Level_Score)

        
        self.add_widget(self.Score_Board)
        self.add_widget(self.Timer)
        #Adds timer and scoreboard to mask

        
       
class TorchEnlarge(Image):
    def __init__(self,**kwargs):
        super(TorchEnlarge, self).__init__(**kwargs)
        self.name = 'Enlarge'
        self.source = 'plus.png'
        self.size_hint = (None,None)

        #Creates Torch enlargement class using 'plus sign' source image

        self.grow_factor = 2
        self.count = 8
        #Sets grow factor and count value (time it will be enlarged)

        self.sound = ''

        
    def collide(self,*args):
        Activate = SoundLoader.load(self.sound)
        
        if Activate:
            Activate.play()
        self.source = 'WHITE.png'
        self.name = 'Empty'
    #Changes source and name
        
        SM.LVS.Level.Level_Mask.Torch.size = [SM.LVS.Level.Level_Mask.Torch.size[0]*self.grow_factor,SM.LVS.Level.Level_Mask.Torch.size[1]*self.grow_factor]
        SM.LVS.Level.Level_Mask.Torch.pos = [(SM.LVS.Level.Level_Mask.GS.Player_Character.center_x - (SM.LVS.Level.Level_Mask.Torch.size[0]/2)),((SM.LVS.Level.Level_Mask.GS.Player_Character.center_y - (SM.LVS.Level.Level_Mask.Torch.size[1]/2)))]

    #Increases size of torch by grow value

        Clock.schedule_interval(self.regular,1)

    #Calls function every second to reduce count value

    def regular(self,dt):
        self.count -=1
        if self.count == 0:
            SM.LVS.Level.Level_Mask.Torch.size = [SM.LVS.Level.Level_Mask.Torch.size[0]/self.grow_factor,SM.LVS.Level.Level_Mask.Torch.size[1]/self.grow_factor]
            SM.LVS.Level.Level_Mask.Torch.pos = [(SM.LVS.Level.Level_Mask.GS.Player_Character.center_x - (SM.LVS.Level.Level_Mask.Torch.size[0]/2)),((SM.LVS.Level.Level_Mask.GS.Player_Character.center_y - (SM.LVS.Level.Level_Mask.Torch.size[1]/2)))]
    #Counts a certain amount of seconds before reverting torch size
            

class GameScreen(Screen):
    def __init__(self,**kwargs):
        super(GameScreen, self).__init__(**kwargs)

        self.Total_Level_Score = 0
        
        self.start_point = []
        self.Maze = self.Maze_create(SM.LVS.Levels_list[SM.LVS.current_level-1])
                                     
        #Creates a variable self.Maze (so it can be called throughout the
        # code), and assigns it the returned list from Maze_create, meaning
        #it's a list of objects. The parameter set for the function is found
        #by referencing the current_level value to find the correct maze list
        for row in self.Maze:
            for maze_object in row:
                if maze_object.name != 'Guard':
                    self.add_widget(maze_object)

        for row in self.Maze:
            for maze_object in row:
                if maze_object.name == 'Guard':
                    self.add_widget(maze_object)
 
        #Iterates through the list of objects, adding each one to the screen
        self.Player_Character = Character(pos = self.start_point,size = [self.C_size,self.C_size])
        self.add_widget(self.Player_Character)

        #Sets size for character according to screen size
        #Creates instance of character and adds it to game screen

        self.add_widget(TimeScore(font_name = 'FFFFORWA.TTF', count = 0))
        #Adds timer to screen

        self.Score_Board = Scoreboard(Total = self.Total_Level_Score)
        self.add_widget(self.Score_Board)
        #Adds scoreboard to screen

            
    def Maze_create(self,maze_list):
        self.block_length = (Window.size[0])/(len(maze_list))
        self.C_size = self.block_length*(float(3)/float(5))
        for i in range(0,len(maze_list)):
            for j in range(0,len(maze_list[i])):
                if maze_list[i][j] == '0': #Empty Space
                    maze_list[i][j] = Empty(size = [self.block_length,self.block_length])
                    maze_list[i][j].pos = (self.block_length*j,self.block_length*i)
                    
                elif maze_list[i][j] == '1': #Wall Block
                    maze_list[i][j] = Block(size = [self.block_length,self.block_length])
                    maze_list[i][j].pos = (self.block_length*j,self.block_length*i)
                    
                elif maze_list[i][j] == '2': #Start Block
                    maze_list[i][j] = Start(size = [self.block_length,self.block_length])
                    maze_list[i][j].pos = (self.block_length*j,self.block_length*i)
                    self.start_point = [maze_list[i][j].pos[0]+2,maze_list[i][j].pos[1]+2]
                    
                elif maze_list[i][j] == '3': #Gem
                    self.add_widget(Empty(size = [self.block_length,self.block_length], pos = [self.block_length*j,self.block_length*i]))
                    maze_list[i][j] = Gem(size = [self.C_size,self.C_size])
                    maze_list[i][j].pos = (self.block_length*j+(self.block_length/6),self.block_length*i+(self.block_length/6))
                    #Creates gem object and sizes and positions it
                    
                    self.Total_Level_Score += 1
                    #Increases variable for total gems in level
                    
                    
                elif maze_list[i][j] == '4': #Horizontal Guard
                    try: #In case guard is in first column, error would be returned, except function executed
                        if maze_list[i][j-1].name == 'Empty': #As block before would be generated before this one
                            self.Direction = -1
                        else:
                            self.Direction = 1
                    except:
                        self.Direction = 1
                    self.add_widget(Empty(size = [self.block_length,self.block_length], pos = [self.block_length*j,self.block_length*i]))
                    maze_list[i][j] = GuardAI(size = [self.C_size,self.C_size])
                    maze_list[i][j].pos = (self.block_length*j+(self.block_length/10),self.block_length*i+(self.block_length/10))
                    maze_list[i][j].direction = [0,self.Direction] #Sets horizontal axis, direction based on empty spaces

                elif maze_list[i][j] == '5': #Vertical Guard
                    try: #In case guard is on bottom row, error would be returned, except function executed
                        if maze_list[i-1][j].name == 'Empty':
                            self.Direction = -1
                        else:
                            self.Direction = 1
                    except:
                        self.Direction = 1
                    self.add_widget(Empty(size = [self.block_length,self.block_length], pos = [self.block_length*j,self.block_length*i]))
                    maze_list[i][j] = GuardAI(size = [self.C_size,self.C_size])
                    maze_list[i][j].pos = (self.block_length*j+(self.block_length/10),self.block_length*i+(self.block_length/10))
                    maze_list[i][j].direction = [1,self.Direction] #Sets direction vertically and based off of empty spaces

                elif maze_list[i][j] == '6':#End Block
                    maze_list[i][j] = End(size = [self.block_length,self.block_length])
                    maze_list[i][j].pos = (self.block_length*j,self.block_length*i)
                    #Sets end block size and position

                elif maze_list[i][j] == '7':#Torch PowerUp
                    self.add_widget(Empty(size = [self.block_length,self.block_length], pos = [self.block_length*j,self.block_length*i]))
                    #Adds empty block as background
                    maze_list[i][j] = TorchEnlarge(size = [self.C_size,self.C_size])
                    maze_list[i][j].pos = (self.block_length*j+(self.block_length/6),self.block_length*i+(self.block_length/6))
                    #Sets Power Up position and size

                    '''maze_list[i][j].sound = 'Tick.MP3' '''


                elif maze_list[i][j] == '8':#Flash PowerUp
                    self.add_widget(Empty(size = [self.block_length,self.block_length], pos = [self.block_length*j,self.block_length*i]))
                    #Adds empty block as background
                    maze_list[i][j] = TorchEnlarge(size = [self.C_size,self.C_size])
                    maze_list[i][j].pos = (self.block_length*j+(self.block_length/6),self.block_length*i+(self.block_length/6))
                    #Sets Power Up position and size

                    maze_list[i][j].source = 'Flash.png'
                    maze_list[i][j].count = 1
                    maze_list[i][j].grow_factor = 1000
                    '''maze_list[i][j].sound = 'Light.MP3' '''

                    #Changes values of class to make the enlarge bigger and last shorter
                    


        return maze_list
        #Iterates through the each list within the main list
        #(every row of the maze), and depending on the string in the list
        #assigns a class object to that position in the list, and gives it
        #a geometric position according to it's list position
                

class Block(Image):
    def __init__(self, **kwargs):
        super(Block, self).__init__(**kwargs)
        self.name = 'Block' #name to be used when analysing objects
        self.source = 'BLOCK.png' #need to create block design
        self.size_hint = (None,None)

class Empty(Image):
    def __init__(self, **kwargs):
        super(Empty, self).__init__(**kwargs)
        self.name = 'Empty'
        self.source = 'WHITE.png'
        self.size_hint = (None,None)
        #Creates 'Empty' class object (the spaces to walk in in the maze)

class Start(Image):
    def __init__(self, **kwargs):
        super(Start, self).__init__(**kwargs)
        self.name = 'Start'
        self.source = 'START.png'
        self.size_hint = (None,None)
        #Start block/position the maze

class Gem(Image):
    def __init__(self, **kwargs):
        super(Gem, self).__init__(**kwargs)
        self.name = 'Gem'
        self.source = 'gem.gif'
        self.size_hint = (None,None)

    def collide(self,*args):
        '''Ping = SoundLoader.load('Ping.MP3')
        Ping.pitch = 2
        if Ping:
            Ping.play()'''
        #Plays 'ping' noise

        args[0].points += 1
        SM.LVS.Level.Level_Mask.GS.Score_Board.updatescore()
        SM.LVS.Level.Level_Mask.Score_Board.updatescore()

        #Updates scoreboards on mask and game screen  
        
        self.source = 'WHITE.png'
        self.name = 'Empty'

        #When character collides with gem, gem dissapears and
        #points added to character score, scoreboard updated
    

class Character(Image):
    def __init__(self, **kwargs):
        super(Character, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(None, self)
        if not self._keyboard:
            return
        self._keyboard.bind(on_key_down=self.on_keyboard_down)

        #calls the keyboard to be used (so key commands are recognised)

        self.name = 'Character'
        self.source = 'characterr.png' #sets initial image source for character
        self.size_hint = (None,None)

        self.points = 0 #Character points

        self.off = 0
        self.collision = False

        #sets all collision variables to show no collisions

        self.prev_x = 0
        self.prev_y = 0
        
        
    def on_keyboard_down(self, keyboard, keycode, text, modifiers):
        try:
            if SM.LVS.Level.Level_Mask.Active == True:
                Move = 1.5*(self.size[0]*(5/3))/25
                self.prev_x = self.pos[0]
                self.prev_y = self.pos[1]
                if keycode[1] == 'left' and self.off != 1:
                    self.x -= Move
                    self.source = 'left.gif'
                elif keycode[1] == 'right' and self.off != 2:
                    self.x += Move
                    self.source = 'right.gif'
                elif keycode[1] == 'down' and self.off != 3:
                    self.y -= Move
                    self.source = 'down.gif'
                elif keycode[1] == 'up' and self.off != 4:
                    self.source = 'up.gif'
                    self.y += Move

            #Checks what key was pressed and changes x and y co-ordinates according
            #to a fraction of the length of one block, and changes character gif to
            #face the right direction, as long as no collisions in that direction
                self.collision = False
                for row in SM.LVS.Level.Level_Mask.GS.Maze:
                    for square in row:
                        if SM.LVS.Level.Level_Mask.GS.Player_Character.collide_widget(square):

                    #Iterates through every object in the maze, checking for collisions

                            if square.name == 'Block':
                                if keycode[1] == 'left':
                                    self.off = 1
                                    self.x = self.prev_x
                                if keycode[1] == 'right':
                                    self.off = 2
                                    self.x = self.prev_x
                                if keycode[1] == 'down':
                                    self.off = 3
                                    self.y = self.prev_y
                                if keycode[1] == 'up':
                                    self.off = 4
                                    self.y = self.prev_y
                                self.collision = True

            #checks if the block is colliding with the character, then checks which
            #key was pressed to cause the collision and stops the character moving
            #any further, as well as bouncing it backwards
                                
                            elif square.name == 'Gem'or square.name == 'Enlarge': #Checks for other collisions
                                square.collide(self) #Execute collision functions for other objects
                            elif square.name == 'End' and self.points == SM.LVS.Level.Level_Mask.GS.Total_Level_Score:
                                square.collide(self) #If all gems collected and end reached, execute end
                        
                if self.collision == False:
                    self.off = 0

                #sets off to 0, in case there are no collisions
                
                SM.LVS.Level.Level_Mask.Torch.pos = [(SM.LVS.Level.Level_Mask.GS.Player_Character.center_x - (SM.LVS.Level.Level_Mask.Torch.size[0]/2)),((SM.LVS.Level.Level_Mask.GS.Player_Character.center_y - (SM.LVS.Level.Level_Mask.Torch.size[1]/2)))]
                #Updates torch position relative to current character
                #size and position
        except:
            self.active = False
class GuardAI(Image):
    def __init__(self,**kwargs):
        super(GuardAI,self).__init__(**kwargs)
        self.source = 'Guard.png'
        self.name  = 'Guard'
        self.size_hint = (None,None)
        self.direction = []
        self.colliding = False
        
        Clock.schedule_interval(self.Movement,0.1)

    def Movement(self, dt):
        
        if self.direction == [0,1]:
            self.source = 'GuardRight.gif'
        elif self.direction == [0,-1]:
            self.source = 'GuardLeft.gif'
        elif self.direction == [1,1]:
            self.source = 'GuardUp.gif'
        elif self.direction == [1,-1]:
            self.source = 'GuardDown.gif'

        #Sets source based on direction
        
        self.pos[self.direction[0]] += (self.direction[1])*(self.size[0]*(5/3)/25)
        #Moves character in selected direction by 'move' amount
        if self.name == 'Guard':
            for row in SM.LVS.Level.Level_Mask.GS.Maze:
                for square in row:
                    if self.collide_widget(square) and square.name == 'Block':
                        self.direction  = [self.direction[0],self.direction[1]*-1]
                        #Changes direction to opposite direction

        #Cycles through maze to see if any collisions and executes function
        #accordingly
        if self.name == 'Guard':
            if self.collide_widget(SM.LVS.Level.Level_Mask.GS.Player_Character):      
                SM.current = 'DefeatScreen'
                
                 
                #will switch screen to defeated screen


class End(Image):
    def __init__(self,**kwargs):
        super(End, self).__init__(**kwargs)
        self.name = 'End'
        self.source = 'End.png'
        self.size_hint = (None,None)

    def collide(self,*args):
        '''EndSound = SoundLoader.load('Mission-Impossible.MP3')

        if EndSound:
            EndSound.play()'''
            
        SM.LVS.Level.Level_Mask.Timer.level_finished = True
        #sets level finished to true, stopping timer

        if SM.SignUp.User.Level_Up_To == SM.LVS.current_level: #Checks if you're at the furthest level you've unlocked
            SM.SignUp.User.Level_Up_To +=1  #Increases level unlocked value

        connection = sqlite3.connect('MazeGameDatabase.db')
        cursor = connection.cursor()
        #Establishes connection to database and creates cursor to execute commands
        Update = ('''UPDATE Users SET LevelUpTo = {Unlocked} WHERE Username = '{Name}' ''').format(Name = SM.SignUp.User.Username,Unlocked = SM.SignUp.User.Level_Up_To )
        cursor.execute(Update)
        connection.commit()#Saves changes to database
        #Updates database to increase what level the user has unlocked up to

        
        SM.LVS.Level.Level_Mask.Timer.Final()
        #Sets final score values
        SM.ES.Update() #Calls label update function
        
        self.name = 'Empty'
        SM.current = 'End'
        #Switches to end screen upon collision
        
class EndScreen(Screen):
    def __init__(self, **kwargs):
        super(EndScreen, self).__init__(**kwargs)
        Box = BoxLayout(orientation = 'vertical')
        LevelButton = ImageButton(source = 'Levels.png',Page ='')
        LevelButton.bind(on_press = self.changer)
        #Creates and binds back button

        Next = ImageButton(source = 'Next.png',Page = '')
        Next.bind(on_press = self.NextLevel)
        #Creates next level button and binds to function

        Retry = ImageButton(source ='Retry.png',Page = '')
        Retry.bind(on_press = self.retry)#Creates retry level button

        ScoreBox = BoxLayout(orientation = 'horizontal')
        
        self.HighScores = BoxLayout(orientation = 'vertical')
        #Creates blank box to be updated with high scores for the current level

        self.YourScores = BoxLayout(orientation = 'vertical')
        self.Score_Label = Label(font_name = 'FFFFORWA.TTF', font_size = 20)
        self.User_HighScore = Label(font_name = 'FFFFORWA.TTF', font_size = 20)
        self.YourScores.add_widget(self.Score_Label)
        self.YourScores.add_widget(self.User_HighScore)
        #Creates blank box to be updated with user's score for the level and their high score for the level

        ScoreBox.add_widget(self.YourScores)
        ScoreBox.add_widget(self.HighScores)

        Buttons = BoxLayout(orientation = 'horizontal')
        
        Buttons.add_widget(Next)
        Buttons.add_widget(Retry)
        Box.add_widget(Buttons)
        Box.add_widget(LevelButton)
        Layout = BoxLayout(orientation = 'vertical')
        Layout.add_widget(Image(source = 'LevelComplete.png'))
        Layout.add_widget(ScoreBox)
        Layout.add_widget(Box)
        self.add_widget(Layout)

    def Update(self):
        
        self.HighScores.clear_widgets()
        #Clears any high scores remaining on end screen from previous level
        
        connection = sqlite3.connect('MazeGameDatabase.db')
        cursor = connection.cursor()
        connection.text_factory = str
        UpdateScores = ('''INSERT INTO Scores (UserScoreID,UserID,LevelID,ScoreTime,ScoreCount)
                        VALUES (NULL,'{ID}',{Level},'{Time}',{Count})''').format(ID = SM.SignUp.User.ID,Level = SM.LVS.current_level,Time = SM.LVS.Level.Level_Mask.Timer.Final_Time,Count = SM.LVS.Level.Level_Mask.Timer.Final_Score)

        cursor.execute(UpdateScores)
        
        #Establishes connection to database then creates new entry in score table with current user's score for the completed level
        connection.commit()#Saves changes to database

        self.Score_Label.text = 'Score:'+(SM.LVS.Level.Level_Mask.Timer.Final_Time)
        #Updates label to have the completed level's score
  
        readScores_unformat = ('''SELECT ScoreTime FROM Scores INNER JOIN Users ON Scores.UserID = Users.UserID WHERE LevelID = {Level} AND Username = '{Name}' ORDER BY ScoreCount ASC''')
        readScores = readScores_unformat.format(Level = SM.LVS.current_level, Name = SM.SignUp.User.Username)
        cursor.execute(readScores)
        UserHighScore = cursor.fetchall()
        self.User_HighScore.text = 'Personal High Score:'+'\n'+UserHighScore[0][0]
        
        #Reads the current user's scores for the completed level
        readScores = ('''SELECT Username, ScoreTime FROM Scores INNER JOIN Users ON Scores.UserID = Users.UserID WHERE LevelID = {Level} ORDER BY ScoreCount ASC''').format(Level = SM.LVS.current_level)
        cursor.execute(readScores)
        LevelScores = cursor.fetchall()
        print(LevelScores)
        #Reads off all scores for the current level into a list, with the usernames of the accounts the scores were obtained by, by joining the scores and users table
        if len(LevelScores) >= 5:
            self.HighScores.add_widget(Label(font_name = 'FFFFORWA.TTF', font_size = 20, text = 'High Scores:'))            
            for i in range(0,5):
                self.HighScores.add_widget(Label(font_name = 'FFFFORWA.TTF', font_size = 20, text = (LevelScores[i][0] + ':' +LevelScores[i][1])))
            

        elif len(LevelScores) < 5:
            self.HighScores.add_widget(Label(font_name = 'FFFFORWA.TTF', font_size = 20, text = 'High Scores:')) 
            for i in range(0,len(LevelScores)):
                self.HighScores.add_widget(Label(font_name = 'FFFFORWA.TTF', font_size = 20, text = (LevelScores[i][0] + ':' +LevelScores[i][1])))

        #Adds top 5 high scores (or less if there aren't 5 scores) to the high scores list             
            
        
    def NextLevel(self,*args):
        SM.LVS.current_level += 1
        SM.DS.Restart()
        #Increases level then calls restart (which will create new level using
        #next level list)

    def changer(self,*args):
        SM.DS.changer() #Calls changer, switches to level screen

    def retry(self,*args):
        SM.DS.Restart()#Restarts level
        
class DefeatScreen(Screen):
    def __init__(self,**kwargs):
        super(DefeatScreen,self).__init__(**kwargs)

        
        Box1 = BoxLayout(orientation = 'horizontal')
        
        Back = ImageButton(source = 'Levels.png',Page = '')
        Back.bind(on_press = self.changer)
        #Creates and binds back button
        
        Replay = ImageButton(source = 'Retry.png', Page = '')
        Replay.bind(on_press = self.Restart)

        #Creates replay button that restarts level

        Box1.add_widget(Replay)
        Box1.add_widget(Back)

        Box2 = BoxLayout(orientation = 'vertical')
        Box2.add_widget(Image(source = 'Defeated.png'))
        Box2.add_widget(Box1)

    #Creates vertical layout of 2 buttons, then adds another button above them

        self.add_widget(Box2)
        
    def Restart(self,*args):
        for row in SM.LVS.Level.Level_Mask.GS.Maze:
            for square in row:
                if square.name == 'Guard' or square.name == 'End':
                    square.name = 'Empty'
                    square.size = [0,0]
                    SM.LVS.Level.Level_Mask.GS.remove_widget(square)

    #Renders all guards and end inactive to prevent collisions after level over

        SM.LVS.Level.Level_Mask.GS.Player_Character.size = [0,0]
        #Reduces character size to 0 to prevent any further collisions
           
        SM.remove_widget(SM.LVS.Level)
        SM.LVS.RunLevel()

    #Removes Gamescreen from screen manager then re-instantiates it in originial
    #Level builder class

    def changer(self,*args):
        for row in SM.LVS.Level.Level_Mask.GS.Maze:
            for square in row:
                if square.name == 'Guard' or square.name == 'End':
                    square.size = [0,0]
                    square.name = 'Empty'
                    SM.LVS.Level.Level_Mask.GS.remove_widget(square)

        #Renders all guards inactive to prevent collisions after level over
        SM.LVS.Level.Level_Mask.GS.Player_Character.size = [0,0]
        #Reduces character size to 0 to prevent any further collisions
        SM.remove_widget(SM.LVS.Level) #Removes game screen from manager
        SM.MS.change_screen()

   #Removes level screen then activates function to remake it

class Scoreboard(Label):
    def __init__(self, **kwargs):
        super(Scoreboard, self).__init__(**kwargs)
        self.font_name = 'FFFFORWA.TTF'
        self.Total = kwargs['Total']
        self.text = 'Score:'+'0'+'/'+str(self.Total)
        #Sets label text to equal the current character score
        #'out of' the total level score
        
        self.font_size = 20
        self.pos = [300,Window.size[1]-70]

        #Sets font size and label position
        


    def updatescore(self):
        self.text = 'Score:'+str(SM.LVS.Level.Level_Mask.GS.Player_Character.points)+'/'+str(SM.LVS.Level.Level_Mask.GS.Total_Level_Score)
    #Updates score when character score increases

class TimeScore(Label):
    def __init__(self, **kwargs):
        super(TimeScore, self).__init__(**kwargs)
        self.count = 0
        self.text = 'Time:' + str(self.count)
        self.font_size = 50
        
        self.level_finished = False #Sets level to be unfinished by default
        
        self.font_size = 20
        self.pos = [40,Window.size[1]-70]
        #Sets size and position

        Clock.schedule_interval(self.update,0.1)
        #Only runs timer if level is currently going
        
    
    def update(self,dt):
        if self.level_finished == False:

            self.count += 0.1
            minutes, seconds = divmod(self.count,60)
            #Converts time into minutes and seconds
        
            miniseconds, whole = math.modf(self.count)
            miniseconds = miniseconds*10
            #Sets 'tenth of seconds' value using fraction part of the count number
        
            self.text = 'Time: ' + '%d:%02d:%02d' %(minutes,seconds,miniseconds)
            #Sets label text to equal custom-made timecode
            self.time = '%d:%02d:%02d' %(minutes,seconds,miniseconds)
    def Final(self,*args):
        self.Final_Score = self.count
        self.Final_Time = self.time

    #Creates label which calls update function every second, increasing by 1
        
                  
class Screens(ScreenManager):
    def __init__(self,**kwargs):
        super(Screens,self).__init__(**kwargs)

        self.transition = NoTransition() #sets 'no animation' transition
        OS = OpenScreen(name = 'OS') #Assigns the OpenScreen Screen to the
                                     #variable name OS (with reference name OS)

        self.MS = MenuScreen(name = 'MENU')
        
        self.LS = LoginScreen(name = 'LOGIN')
        self.SignUp = SignUpScreen(name = 'SIGNUP')
        
        
        self.DS = DefeatScreen(name = 'DefeatScreen')
        
        self.ES = EndScreen(name = 'End')

        self.TUT = TutorialScreen(name = 'TUTORIAL')

        #Implements various screen classes
        self.add_widget(OS)
        self.add_widget(self.TUT)
        self.add_widget(self.MS)
        self.add_widget(self.LS)
        self.add_widget(self.SignUp)
        self.add_widget(self.DS)        
        self.add_widget(self.ES)
        

        #Adds Screens to screen manager

class GameApp(App):
    def build(self):
        Window.size = (650,650)
        global SM
        SM = Screens() #Empty screen managing widget class
        return SM  #Means that when app class is called/ran, SM is run

if __name__ == '__main__':
    '''Music = SoundLoader.load('Mission-Impossible.MP3')
    Music.volume = 0.6
    if Music:
            Music.play()'''
    #Plays background music
            
    GameApp().run()
    
    #Runs the main App
    
